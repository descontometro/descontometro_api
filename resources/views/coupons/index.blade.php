@extends('layouts.restricted')

@section('restricted_content')
    <div class="card">
        <div class="card-header">Dashboard</div>

        <div class="card-body">
            <div class="col-md-12">
                <div class="row">
                    <div class="col-md-8">
                        <a href="{{ route('coupons.create') }}"><i class="fas fa-plus"></i> Novo</a>
                    </div>
                    <div class="col-md-4">
                        <form>
                            <div class="form-group">
                                <input type="search" class="form-control" placeholder="Buscar">
                            </div>
                        </form>
                    </div>
                </div>
            </div>

            <table class="table table-striped table-hover">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>Imagem</th>
                        <th>Título</th>
                        <th>Ações</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($coupons as $value)
                    <tr>
                        <td>{{ $value->id }}</td>
                        <td><img src="{{ asset('storage/coupons/'.$value->image) }}" style="width:100px"></td>
                        <td>{{ $value->name }}</td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
        <div class="card-footer">
            {{ $coupons->links() }}
        </div>
    </div>
@endsection
