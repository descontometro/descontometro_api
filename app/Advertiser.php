<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Advertiser extends Model
{
    protected $fillable = [
        'user_id',
        'social_reason',
        'state_registration',
        'cnpj'
    ];
}
