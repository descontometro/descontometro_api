<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Coupon;
use App\Helpers\FileUploadHelper;
use Validator;

class CouponsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $coupons = Coupon::orderBy('created_at', 'DESC')->paginate();
        
        return view('coupons.index', compact('coupons'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('coupons.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validatedData = $this->validation($request);
        
        $fileName = FileUploadHelper::uploadFile($request->image, 'coupons');

        $advertiser = $request->user()->advertiser()->first();

        $coupon = Coupon::create([
            'advertiser_id' => $advertiser->id,
            'name' => $validatedData['name'],
            'image' => $fileName,
            'description' => $validatedData['description'],
            'rules' => $validatedData['rules'],
            'start_date' => $validatedData['start_date'],
            'end_date' => isset($validatedData['end_date']) ? $validatedData['end_date'] : null,
            'max_codes' => isset($validatedData['max_codes']) ? $validatedData['max_codes'] : null,
        ]);

        return response()->json(['success' => $coupon], 200);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    private function validation(Request $request){
        $validatedData = $request->validate([
            'name' => 'required|max:255',
            'image' => 'required|file|image',
            'start_date' => 'required|date_format:d/m/Y H:i',
            'rules' => 'nullable',
            'description' => 'required',
            'end_date' => 'nullable||date_format:d/m/Y H:i',
            'max_codes' => 'nullable|numeric',
        ]);

        $validatedData['start_date'] = date('Y-m-d H:i:s', strtotime(str_replace('/', '-', $validatedData['start_date'])));
        if(isset($validatedData['end_date']))
            $validatedData['end_date'] = date('Y-m-d H:i:s', strtotime(str_replace('/', '-', $validatedData['end_date'])));

        return $validatedData;
    }
}
