@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-3">
            <ul class="list-group">
                <a href="{{ route('dashboard') }}" class="list-group-item"><i class="fa fa-dashboard"></i> Home</a>
                <a href="{{ route('coupons.index') }}" class="list-group-item"><i class="fa fa-ticket"></i> Promoções</a>
            </ul>
        </div>
        <div class="col-md-9">
            @yield('restricted_content')
        </div>
    </div>
</div>
@endsection
