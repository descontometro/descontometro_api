@extends('layouts.restricted')

@section('restricted_content')
    <the-coupons-form-card
        title="Nova Promoção"
        action="{{ route('coupons.store') }}"
        ></the-coupons-form-card>
@endsection
