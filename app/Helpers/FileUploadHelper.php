<?php
namespace App\Helpers;

class FileUploadHelper {
    public static function uploadFile($file, $path){
        $newName = uniqid(date('HisYmd'));
        $extension = $file->extension();
        $fileName = "{$newName}.{$extension}";

        $upload = $file->storeAs('public/'.$path, $fileName);
      
        if ( !$upload )
            return false;

        return $fileName;  
    }
}
