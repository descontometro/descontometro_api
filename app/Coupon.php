<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Coupon extends Model
{
    use SoftDeletes;
    public $timestamps = true;

    protected $fillable = [
        'advertiser_id',
        'name',
        'image',
        'description',
        'rules',
        'start_date',
        'end_date',
        'max_codes'
    ];


}
